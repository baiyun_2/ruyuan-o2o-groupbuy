package com.ruyuan.o2o.groupbuy.account.admin.controller;

import com.ruyuan.o2o.groupbuy.account.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商户登录
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/auth")
@Slf4j
@Api(tags = "商户登录")
public class HttpController {

    @Autowired
    private LoginService loginService;

    /**
     * 商户登录接口
     *
     * @param adminName 管理员名称
     * @param passwd    管理员密码
     * @return
     */
    @PostMapping("/login")
    @ApiOperation("商户登录")
    public String login(String adminName, String passwd) {
        if (!loginService.login(adminName, passwd)) {
            return "fail";
        }
        log.info("用户:{} 登录成功", adminName);
        return "success";
    }

}

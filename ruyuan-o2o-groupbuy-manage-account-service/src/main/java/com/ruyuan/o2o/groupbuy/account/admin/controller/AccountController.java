package com.ruyuan.o2o.groupbuy.account.admin.controller;

import com.ruyuan.o2o.groupbuy.account.vo.AccountRoleVO;
import com.ruyuan.o2o.groupbuy.account.vo.AccountVO;
import com.ruyuan.o2o.groupbuy.account.service.AccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商户账号
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/account")
@Slf4j
@Api(tags = "商户账号")
public class AccountController {

    @Reference(version = "1.0.0", interfaceClass = AccountService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private AccountService accountService;

    /**
     * 创建账号
     *
     * @param accountVO 前台表单
     * @return 创建账号结果
     */
    @PostMapping("/save")
    @ApiOperation("创建商户管理员账号")
    public Boolean save(@RequestBody AccountVO accountVO) {
        accountService.save(accountVO);
        log.info("创建管理账号:{} 完成", accountVO.getAdminName());
        return Boolean.TRUE;
    }

    /**
     * 更新账号
     *
     * @param accountVO 前台表单
     * @return
     */
    @PostMapping("/update")
    @ApiOperation("更新商户管理员账号")
    public Boolean update(@RequestBody AccountVO accountVO) {
        accountService.update(accountVO);
        log.info("更新管理账号:{} 完成", accountVO.getAdminName());
        return Boolean.TRUE;
    }

    /**
     * 关闭账号
     *
     * @param adminId 账号id
     * @return
     */
    @PostMapping("/close/{adminId}")
    @ApiOperation("关闭商户管理员账号")
    public Boolean close(@PathVariable("adminId") Integer adminId) {
        if (!accountService.close(adminId)) {
            return Boolean.FALSE;
        }
        log.info("关闭管理账号:{} 完成", adminId);
        return Boolean.TRUE;
    }

    /**
     * 开启账号
     *
     * @param adminId 账号id
     * @return
     */
    @PostMapping("/open/{adminId}")
    @ApiOperation("开启商户管理员账号")
    public Boolean open(@PathVariable("adminId") Integer adminId) {
        if (!accountService.open(adminId)) {
            return Boolean.FALSE;
        }
        log.info("开启管理账号:{} 完成", adminId);
        return Boolean.TRUE;
    }

    /**
     * 分页查询账号列表
     *
     * @param pageNum  页数
     * @param pageSize 每页展示数据数量
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("查询管理账号列表")
    public List<AccountVO> page(Integer pageNum, Integer pageSize) {
        List<AccountVO> accountVos = accountService.listByPage(pageNum, pageSize);
        return accountVos;
    }

    /**
     * 根据id查询账号
     *
     * @param adminId 账号id
     * @return
     */
    @GetMapping("/{adminId}")
    @ApiOperation("查询管理账号详情")
    public AccountVO page(@PathVariable("adminId") Integer adminId) {
        AccountVO accountVO = accountService.findById(adminId);
        return accountVO;
    }

    /**
     * 账号绑定角色
     *
     * @param accountRoleVO 前台表单
     * @return
     */
    @PostMapping("/bind/role")
    @ApiOperation("账号绑定角色")
    public String bindRole(@RequestBody AccountRoleVO accountRoleVO) {
        Integer adminId = accountRoleVO.getAdminId();
        Integer roleId = accountRoleVO.getRoleId();
        if (!accountService.accountBindRole(accountRoleVO)) {
            return "fail";
        }
        log.info("用户:{},角色:{} 绑定成功", adminId, roleId);
        return "success";
    }
}

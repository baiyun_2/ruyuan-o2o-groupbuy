package com.ruyuan.o2o.groupbuy.item.service;

import com.ruyuan.o2o.groupbuy.item.vo.ItemVO;

import java.util.List;

/**
 * 商品service组件接口
 *
 * @author ming qian
 */
public interface ItemService {
    /**
     * 创建商品
     *
     * @param itemVO
     */
    void save(ItemVO itemVO);

    /**
     * 更新商品
     *
     * @param itemVO
     */
    void update(ItemVO itemVO);

    /**
     * 下架商品
     *
     * @param itemId
     */
    void offLine(Integer itemId);

    /**
     * 上架商品
     *
     * @param itemId
     */
    void onLine(Integer itemId);

    /**
     * 分页查询商品
     *
     * @param pageNum  查询页数
     * @param pageSize 每次查询数
     * @return 查询列表
     */
    List<ItemVO> listByPage(Integer pageNum, Integer pageSize);

    /**
     * 根据id查询商品
     *
     * @param itemId 商品id
     * @return 商品vo
     */
    ItemVO findById(Integer itemId);

    /**
     * 删除商品
     *
     * @param itemId 商品id
     */
    void delete(Integer itemId);

    /**
     * 用户端商品详情
     *
     * @param itemId 商品id
     * @return 商品json信息
     */
    String findDetailById(Integer itemId);
}

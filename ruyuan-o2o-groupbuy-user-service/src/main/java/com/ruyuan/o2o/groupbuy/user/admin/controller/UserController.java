package com.ruyuan.o2o.groupbuy.user.admin.controller;

import com.ruyuan.o2o.groupbuy.user.service.UserService;
import com.ruyuan.o2o.groupbuy.user.vo.UserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 消费者用户管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/user")
@Slf4j
@Api(tags = "消费者用户管理")
public class UserController {

    @Reference(version = "1.0.0", interfaceClass = UserService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private UserService userService;

    /**
     * 用户注册
     *
     * @param userVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("用户注册")
    public Boolean save(@RequestBody UserVO userVO) {
        userService.save(userVO);
        log.info("用户注册:{} 完成", userVO.getUserPhone());
        return Boolean.TRUE;
    }
}

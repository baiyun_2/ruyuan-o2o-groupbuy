package com.ruyuan.o2o.groupbuy.pay.service;


import com.ruyuan.o2o.groupbuy.pay.vo.PayUserVO;

import java.util.List;

/**
 * 用户消费记录service组件接口
 *
 * @author ming qian
 */
public interface PayUserService {

    /**
     * 分页查询用户消费记录信息
     *
     * @param pageNum  查询页数
     * @param pageSize 每次查询数
     * @return 查询列表
     */
    List<PayUserVO> listByPage(Integer pageNum, Integer pageSize);

}

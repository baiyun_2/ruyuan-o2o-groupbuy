package com.ruyuan.o2o.groupbuy.record.service;

import com.ruyuan.o2o.groupbuy.record.vo.RecordVO;

/**
 * 消费者用户操作记录服务service组件接口
 *
 * @author ming qian
 */
public interface RecordService {
    /**
     * 保存操作记录
     * 可在多个关键节点位置记录操作行为
     * 目前模拟在立即购买时记录一次
     *
     * @param recordVO
     */
    void save(RecordVO recordVO);
}

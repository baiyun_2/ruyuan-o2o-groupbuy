package com.ruyuan.o2o.groupbuy.common;

import org.dozer.DozerBeanMapper;

/**
 * 对象拷贝工具类
 *
 * @author ming qian
 */
public class BeanMapper {
    private static DozerBeanMapper dozer = new DozerBeanMapper();

    public static void copy(Object source, Object destinationObject) {
        if(source != null) {
            dozer.map(source, destinationObject);
        }
    }
}

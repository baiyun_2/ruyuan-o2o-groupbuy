package com.ruyuan.o2o.groupbuy.common;

/**
 * excel路径常量
 *
 * @author ming qian
 */
public class Excel {
    public static final String EXCEL_PATH = "D:\\excel\\";
    public static final String EXCEL_SUFFIX = ".xlsx";
}

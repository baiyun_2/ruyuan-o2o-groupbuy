package com.ruyuan.o2o.groupbuy.account.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 商户角色VO类
 *
 * @author ming qian
 */
@ApiModel(value = "商户角色VO类")
@Getter
@Setter
@ToString
public class RoleVO implements Serializable {
    private static final long serialVersionUID = -7832132890351837460L;

    /**
     * 角色id
     */
    @ApiModelProperty("角色id")
    private Integer roleId;

    /**
     * 角色名称
     */
    @ApiModelProperty("角色名称")
    private String roleName;

    /**
     * 角色类型
     */
    @ApiModelProperty("角色类型")
    private Integer roleType;

}

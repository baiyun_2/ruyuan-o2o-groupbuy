package com.ruyuan.o2o.groupbuy.account.service;

import com.ruyuan.o2o.groupbuy.account.vo.AuthVO;

import java.util.List;

/**
 * 权限管理service组件接口
 *
 * @author ming qian
 */
public interface AuthService {
    /**
     * 创建权限
     *
     * @param authVO
     */
    void save(AuthVO authVO);

    /**
     * 分页查询账号
     *
     * @param pageNum  查询页数
     * @param pageSize 每次查询数
     * @return 查询列表
     */
    List<AuthVO> listByPage(Integer pageNum, Integer pageSize);

}

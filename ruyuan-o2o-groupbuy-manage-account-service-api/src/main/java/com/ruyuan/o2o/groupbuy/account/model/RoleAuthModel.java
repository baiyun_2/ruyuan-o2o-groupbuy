package com.ruyuan.o2o.groupbuy.account.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 角色权限绑定关系实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class RoleAuthModel {
    /**
     * 主键id
     */
    private Integer id;
    /**
     * 角色id
     */
    private Integer roleId;
    /**
     * 权限id
     */
    private String authIds;

}

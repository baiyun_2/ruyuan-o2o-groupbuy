package com.ruyuan.o2o.groupbuy.batch.admin.task;

import com.ruyuan.o2o.groupbuy.excel.service.ExcelService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

/**
 * 批处理任务执行器
 *
 * @author ming qian
 */
@Component
@Slf4j
public class JobHandler {

  @Reference(
      version = "1.0.0",
      interfaceClass = ExcelService.class,
      cluster = "failfast",
      loadbalance = "roundrobin")
  private ExcelService excelService;

  /**
   * 模拟自动生成报表 执行时间可以在xxl-job控制台配置
   *
   * @return 执行结果
   */
  @XxlJob(value = "jobHandler")
  public ReturnT<String> execute() {
    excelService.excel();
    log.info("已生成报表");
    return ReturnT.SUCCESS;
  }
}

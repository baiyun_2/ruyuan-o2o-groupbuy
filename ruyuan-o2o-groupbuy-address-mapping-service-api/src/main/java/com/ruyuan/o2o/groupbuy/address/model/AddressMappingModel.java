package com.ruyuan.o2o.groupbuy.address.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 省市区映射服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class AddressMappingModel implements Serializable {
    private static final long serialVersionUID = -8912228285874184550L;

    /**
     * 省id
     */
    private Integer provinceId;
    /**
     * 省名称
     */
    private String provinceName;
    /**
     * 市id
     */
    private Integer cityId;
    /**
     * 市名称
     */
    private String cityName;
    /**
     * 区id
     */
    private Integer districtId;
    /**
     * 区名称
     */
    private String districtName;
}
package com.ruyuan.o2o.groupbuy.pay.http;

import com.ruyuan.o2o.groupbuy.pay.service.PayService;
import com.ruyuan.o2o.groupbuy.pay.vo.PayVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单支付
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/pay")
@Slf4j
@Api(tags = "订单支付")
public class PayHttpController {

    @Reference(version = "1.0.0", interfaceClass = PayService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private PayService payService;

    /**
     * 创建支付信息
     * 支付成功自动发放奖励积分
     *
     * @param payVO 前台表单
     * @return
     */
    @PostMapping("/order")
    @ApiOperation("提交支付")
    public Boolean save(@RequestBody PayVO payVO) {
        return payService.save(payVO);
    }
}

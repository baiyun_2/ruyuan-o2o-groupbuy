package com.ruyuan.o2o.groupbuy.pay.admin.service;

import com.github.pagehelper.PageHelper;
import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.pay.admin.dao.PayDao;
import com.ruyuan.o2o.groupbuy.pay.admin.dao.PayUserDao;
import com.ruyuan.o2o.groupbuy.pay.model.PayModel;
import com.ruyuan.o2o.groupbuy.pay.model.PayUserModel;
import com.ruyuan.o2o.groupbuy.pay.service.PayService;
import com.ruyuan.o2o.groupbuy.pay.service.PayUserService;
import com.ruyuan.o2o.groupbuy.pay.vo.PayUserVO;
import com.ruyuan.o2o.groupbuy.pay.vo.PayVO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户消费记录service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = PayUserService.class, cluster = "failfast", loadbalance = "roundrobin")
public class PayUserServiceImpl implements PayUserService {

    @Autowired
    private PayUserDao payUserVO;

    /**
     * 分页查询支付信息
     *
     * @return
     */
    @Override
    public List<PayUserVO> listByPage(Integer pageNum, Integer pageSize) {
        List<PayUserVO> payUserVoS = new ArrayList<>();

        PageHelper.startPage(pageNum, pageSize);
        List<PayUserModel> payUserModels = payUserVO.listByPage();

        for (PayUserModel payUserModel : payUserModels) {
            PayUserVO payUserVO = new PayUserVO();
            BeanMapper.copy(payUserModel, payUserVO);
            payUserVoS.add(payUserVO);
        }

        return payUserVoS;
    }

}

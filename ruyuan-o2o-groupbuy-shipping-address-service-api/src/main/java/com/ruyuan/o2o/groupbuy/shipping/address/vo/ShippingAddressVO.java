package com.ruyuan.o2o.groupbuy.shipping.address.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户收货地址服务VO类
 *
 * @author ming qian
 */
@ApiModel(value = "用户收货地址服务VO类")
@Getter
@Setter
@ToString
public class ShippingAddressVO implements Serializable {
    private static final long serialVersionUID = -8485498832087967441L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Integer id;

    /**
     * 消费者用户id
     */
    @ApiModelProperty("消费者用户id")
    private Integer userId;

    /**
     * 省id
     */
    @ApiModelProperty("省id")
    private Integer provinceId;

    /**
     * 市id
     */
    @ApiModelProperty("市id")
    private Integer cityId;

    /**
     * 区id
     */
    @ApiModelProperty("区id")
    private Integer districtId;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private String createTime;

    /**
     * 详细地址
     */
    @ApiModelProperty("详细地址")
    private String detailAddress;
}
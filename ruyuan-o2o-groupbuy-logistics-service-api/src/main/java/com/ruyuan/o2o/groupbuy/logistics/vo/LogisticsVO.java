package com.ruyuan.o2o.groupbuy.logistics.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 物流配送服务VO类
 *
 * @author ming qian
 */
@ApiModel(value = "物流配送服务VO类")
@Getter
@Setter
@ToString
public class LogisticsVO implements Serializable {
    private static final long serialVersionUID = 8666270947554008987L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Integer id;

    /**
     * 门店id
     */
    @ApiModelProperty("门店id")
    private Integer storeId;

    /**
     * 消费者用户id
     */
    @ApiModelProperty("消费者用户id")
    private Integer userId;

    /**
     * 订单id
     */
    @ApiModelProperty("订单id")
    private String orderId;

    /**
     * 支付id
     */
    @ApiModelProperty("支付id")
    private Integer payId;

    /**
     * 收货人昵称
     */
    @ApiModelProperty("收货人昵称")
    private Integer consigneeName;

    /**
     * 收货人手机号
     */
    @ApiModelProperty("收货人手机号")
    private Integer consigneePhone;

    /**
     * 订单物流状态 0 门店自配送 1 物流配送
     */
    @ApiModelProperty("订单物流状态 0 门店自配送 1 物流配送")
    private Integer orderLogisticsType;

    /**
     * 门店送货人
     */
    @ApiModelProperty("门店送货人")
    private String deliveryName;

    /**
     * 门店送货人手机号
     */
    @ApiModelProperty("门店送货人手机号")
    private Integer deliveryPhone;

    /**
     * 物流公司名称
     */
    @ApiModelProperty("物流公司名称")
    private String logisticsName;

    /**
     * 物流单号
     */
    @ApiModelProperty("物流单号")
    private Integer logisticsNo;

    /**
     * 订单物流状态 0 已配送 1 配送完成 2 配送失败
     */
    @ApiModelProperty("订单物流状态 0 已配送 1 配送完成 2 配送失败")
    private Integer orderLogisticsResult;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private String createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    private String updateTime;

    /**
     * 收货人地址配送
     */
    @ApiModelProperty("收货人地址配送")
    private String consigneeAddress;

    /**
     * 配送失败原因
     */
    @ApiModelProperty("配送失败原因")
    private String orderLogisticsFailedInfo;
}
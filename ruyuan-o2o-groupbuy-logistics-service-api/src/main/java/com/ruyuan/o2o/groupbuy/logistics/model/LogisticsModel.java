package com.ruyuan.o2o.groupbuy.logistics.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 物流配送服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class LogisticsModel implements Serializable {
    private static final long serialVersionUID = 4897330988884145152L;

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 门店id
     */
    private Integer storeId;

    /**
     * 消费者用户id
     */
    private Integer userId;

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 支付id
     */
    private Integer payId;

    /**
     * 收货人昵称
     */
    private Integer consigneeName;

    /**
     * 收货人手机号
     */
    private Integer consigneePhone;

    /**
     * 订单物流状态 0 门店自配送 1 物流配送
     */
    private Integer orderLogisticsType;

    /**
     * 门店送货人
     */
    private String deliveryName;

    /**
     * 门店送货人手机号
     */
    private Integer deliveryPhone;

    /**
     * 物流公司名称
     */
    private String logisticsName;

    /**
     * 物流单号
     */
    private Integer logisticsNo;

    /**
     * 订单物流状态 0 已配送 1 配送完成 2 配送失败
     */
    private Integer orderLogisticsResult;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 收货人地址配送
     */
    private String consigneeAddress;

    /**
     * 配送失败原因
     */
    private String orderLogisticsFailedInfo;
}
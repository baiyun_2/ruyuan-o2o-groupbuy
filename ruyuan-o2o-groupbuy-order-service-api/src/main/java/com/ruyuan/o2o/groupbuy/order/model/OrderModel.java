package com.ruyuan.o2o.groupbuy.order.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class OrderModel implements Serializable {
    private static final long serialVersionUID = -7281874177857473153L;

    /**
     * 主键id
     */
    private Integer id;
    /**
     * 订单id
     */
    private String orderId;
    /**
     * 消费者用户id
     */
    private Integer userId;
    /**
     * 门店id
     */
    private Integer storeId;
    /**
     * 商户id
     */
    private Integer shopId;
    /**
     * 商品id
     */
    private Integer itemId;
    /**
     * 商品购买数量
     */
    private Integer itemNum;
    /**
     * 活动id
     */
    private Integer promotionsId;
    /**
     * 优惠券id
     */
    private Integer couponsId;
    /**
     * 订单实际金额
     */
    private Double orderPrice;
    /**
     * 订单类型 0 到店消费 1配送
     */
    private Integer orderType;
    /**
     * 订单状态 10 待付款 11 已超时 12 支付成功 13 支付失败 14 已发货 15 已签收 16 拒绝签收 17 无人签收 18 订单取消
     */
    private Integer orderStatus;
    /**
     * 创建时间
     */
    private String createTime;
}
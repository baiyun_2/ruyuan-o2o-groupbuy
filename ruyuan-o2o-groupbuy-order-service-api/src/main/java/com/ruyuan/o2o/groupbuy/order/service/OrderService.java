package com.ruyuan.o2o.groupbuy.order.service;


import com.ruyuan.o2o.groupbuy.order.dto.OrderExcelDTO;
import com.ruyuan.o2o.groupbuy.order.model.OrderModel;
import com.ruyuan.o2o.groupbuy.order.vo.OrderBuyVO;
import com.ruyuan.o2o.groupbuy.order.vo.OrderVO;

import java.util.List;

/**
 * 订单服务service组件接口
 *
 * @author ming qian
 */
public interface OrderService {
    /**
     * 生成订单
     *
     * @param orderBuyVO
     */
    void save(OrderBuyVO orderBuyVO);

    /**
     * 分页查询订单
     *
     * @param pageNum  查询页数
     * @param pageSize 每次查询数
     * @return 查询列表
     */
    List<OrderVO> listByPage(Integer pageNum, Integer pageSize);

    /**
     * 根据id查询订单
     *
     * @param orderId 订单id
     * @return 订单vo
     */
    OrderVO findById(String orderId);

    /**
     * 查询所有数据
     *
     * @return 数据list
     */
    List<OrderExcelDTO> selectAll();
}

package com.ruyuan.o2o.groupbuy.order.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 订单服务dto类 用于提供报表数据打印
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class OrderExcelDTO implements Serializable {
    private static final long serialVersionUID = -5767669549425380492L;

    /**
     * 主键id
     */
    @ExcelProperty("主键id")
    private Integer id;

    /**
     * 订单id
     */
    @ExcelProperty("订单id")
    private String orderId;

    /**
     * 消费者用户id
     */
    @ExcelProperty("消费者用户id")
    private Integer userId;

    /**
     * 门店id
     */
    @ExcelProperty("门店id")
    private Integer storeId;

    /**
     * 商户id
     */
    @ExcelProperty("商户id")
    private Integer shopId;

    /**
     * 商品id
     */
    @ExcelProperty("商品id")
    private Integer itemId;

    /**
     * 商品购买数量
     */
    @ExcelProperty("商品购买数量")
    private Integer itemNum;

    /**
     * 活动id
     */
    @ExcelProperty("活动id")
    private Integer promotionsId;

    /**
     * 优惠券id
     */
    @ExcelProperty("优惠券id")
    private Integer couponsId;

    /**
     * 订单实际金额
     */
    @ExcelProperty("订单实际金额")
    private Double orderPrice;

    /**
     * 订单类型 0 到店消费 1配送
     */
    @ExcelProperty("订单类型")
    private Integer orderType;

    /**
     * 订单状态 10 待付款 11 已超时 12 支付成功 13 支付失败 14 已发货 15 已签收 16 拒绝签收 17 无人签收 18 订单取消
     */
    @ExcelProperty("订单状态")
    private Integer orderStatus;

    /**
     * 创建时间
     */
    @ExcelProperty("创建时间")
    private String createTime;
}
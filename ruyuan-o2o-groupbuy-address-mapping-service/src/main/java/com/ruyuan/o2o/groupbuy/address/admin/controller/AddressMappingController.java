package com.ruyuan.o2o.groupbuy.address.admin.controller;

import com.ruyuan.o2o.groupbuy.address.service.AddressMappingService;
import com.ruyuan.o2o.groupbuy.address.vo.AddressMappingVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 省市区管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/address")
@Slf4j
@Api(tags = "省市区管理")
public class AddressMappingController {

  @Reference(
      version = "1.0.0",
      interfaceClass = AddressMappingService.class,
      cluster = "failfast",
      loadbalance = "roundrobin")
  private AddressMappingService addressMappingService;

  /**
   * 创建地址映射
   *
   * @param addressMappingVO 前台表单
   * @return 返回结果
   */
  @PostMapping("/save")
  @ApiOperation("创建地址映射")
  public String save(@RequestBody AddressMappingVO addressMappingVO) {
    if (!addressMappingService.save(addressMappingVO)) {
      return "failed";
    }
    log.info("创建地址映射完成");
    return "success";
  }

  /**
   * 更新地址映射
   *
   * @param addressMappingVO 前台表单
   * @return 返回结果
   */
  @PostMapping("/update")
  @ApiOperation("更新地址映射")
  public Boolean update(@RequestBody AddressMappingVO addressMappingVO) {
    addressMappingService.update(addressMappingVO);
    log.info("更新地址映射完成");
    return Boolean.TRUE;
  }

  /**
   * 查询映射列表
   *
   * @param pageNum 页数
   * @param pageSize 每页展示数据数量
   * @return 查询列表
   */
  @GetMapping("/page")
  @ApiOperation("查询映射列表")
  public List<AddressMappingVO> page(Integer pageNum, Integer pageSize) {
    return addressMappingService.listByPage(pageNum, pageSize);
  }
}

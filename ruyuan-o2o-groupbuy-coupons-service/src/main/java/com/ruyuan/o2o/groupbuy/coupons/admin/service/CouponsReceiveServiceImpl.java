package com.ruyuan.o2o.groupbuy.coupons.admin.service;

import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.coupons.admin.dao.CouponsReceiveDao;
import com.ruyuan.o2o.groupbuy.coupons.model.CouponsReceiveModel;
import com.ruyuan.o2o.groupbuy.coupons.service.CouponsReceiveService;
import com.ruyuan.o2o.groupbuy.coupons.vo.CouponsReceiveVO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 处理优惠券服务的service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = CouponsReceiveService.class, cluster = "failfast", loadbalance = "roundrobin")
public class CouponsReceiveServiceImpl implements CouponsReceiveService {

    @Autowired
    private CouponsReceiveDao couponsReceiveDao;

    /**
     * 用户领取优惠券
     *
     * @param couponsReceiveVO
     */
    @Override
    public Boolean receiveCoupons(CouponsReceiveVO couponsReceiveVO) {
        CouponsReceiveModel couponsReceiveModel = new CouponsReceiveModel();
        BeanMapper.copy(couponsReceiveVO, couponsReceiveModel);
        couponsReceiveModel.setReceiveTime(TimeUtil.format(System.currentTimeMillis()));
        couponsReceiveDao.save(couponsReceiveModel);
        return Boolean.TRUE;
    }
}

package com.ruyuan.o2o.groupbuy.image;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 图片服务启动类
 *
 * @author ming qian
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ImageMain {
    public static void main(String[] args) {
        SpringApplication.run(ImageMain.class, args);
    }
}
